unit BinaryProtocolCommon;

interface

uses
  packages.tools.VersionInformation,
  System.Classes, System.SysUtils, System.Types, SyncObjs;

const

  NC_BPCOMMANDSTATE_REQUEST           = 0;
  NC_BPCOMMANDSTATE_OK                = 1;
  NC_BPCOMMANDSTATE_FAIL              = -1;
  NC_BPCOMMANDSTATE_FAILPROCESSING    = -2;

type

  TBinaryProtocolCommand = class;

  EBinaryProtocol = class(Exception)
  end;

  EBinaryProtocolCommand = class(EBinaryProtocol)
  end;

  TBinaryProtocolCommandHeaderIdent = array[0..4] of Byte;
  RBinaryProtocolCommandHeader = packed record
    //HeaderIdent   : TBinaryProtocolCommandHeaderIdent;
    CommandType   : Int64;
    CommandIdent  : TGUID;
    CommandState  : Integer;
    CommandLength : Integer;
  end;
  PRBinaryProtocolCommandHeader = ^RBinaryProtocolCommandHeader;

  IBinaryProtocolCommandProcessable = interface
  ['{4C03BD6A-DCB5-4913-92B9-15C80D385BE0}']
    procedure process(aContext : TObject; aData : TBytes);
  end;

  TBinaryProtocolCommandProcessAnswerEvent = procedure(Sender : TObject; aHeaderData, aCommandData : TBytes) of object;
  TBinaryProtocolCommandProcessEvent = procedure(Sender : TBinaryProtocolCommand; aContext : TObject) of object;

  TBinaryProtocolCommand = class(TComponent)
  protected
    fHeaderIdentRequest     : String;
    fHeaderIdentAnswer      : String;
    fHeader                 : RBinaryProtocolCommandHeader;
    fCommandType            : Integer;
    fCommandIdent           : TGUID;
    fCommandState           : Integer;
    fCommandStateText       : String;
    fCommandWaitEvent       : TEvent;
    fCommandTimeOut         : Cardinal;
    fOnProcessCommandAnswer : TBinaryProtocolCommandProcessAnswerEvent;
    fOnProcessCommandStart  : TBinaryProtocolCommandProcessEvent;
    fOnProcessCommandFinish : TBinaryProtocolCommandProcessEvent;
    fTagDouble              : Double;
    fTagString              : String;

    function  getCommandHeaderLengthRequest : Integer;
    function  getCommandHeaderLengthAnswer : Integer;
    procedure writeHeaderRequest(aData : TBytes);
    procedure writeHeaderAnswer(aData : TBytes; aState : Integer);

    procedure define(aCommandType: Integer;
                    aCommandLength : Integer; aCommandIdent : TGUID); overload; virtual;
    procedure define(aCommandType: Integer;
                    aCommandLength : Integer); overload; virtual;

    class function createCommandHeader(aCommandType: Integer; aCommandIdent : TGUID;
                    aCommandLength : Integer): RBinaryProtocolCommandHeader;
    function  processCommandAnswer(aHeaderData, aCommandData : TBytes) : Boolean; virtual;

    function  combineRequestData(aData : TBytes) : TBytes; virtual;
    function  combineAnswerData(aData : TBytes) : TBytes; virtual;

    procedure informProcessCommandStart(aContext : TObject); virtual;
    procedure informProcessCommandFinish(aContext : TObject); virtual;
  public
    class function  checkHeaderIdent(aHeaderData : TBytes; aHeaderIdent : String) : Boolean;

    constructor Create(AOwner : TComponent); override;
    constructor CreateIdented(AOwner : TComponent; aIdent : TGUID); virtual;
    destructor Destroy; override;

    procedure defineHeaderIdents(aHeaderIdentRequest, aHeaderIdentAnswer : String); virtual;

    procedure waitFor(aTimeOutMS : Cardinal = 0);
    procedure informCommandAnswer(aHeaderData, aCommandData : TBytes); virtual;
    procedure informCommandSend; virtual;

    procedure setCommandToFailState(aCommandState : Integer; aCommandStateText : String); virtual;
    function  getCommandStateText : String; virtual;

    function  toBytesRequest : TBytes; virtual; abstract;

    function  toBytesAnswer : TBytes; virtual; abstract;
    function  isSuccessfully : Boolean;

    property CommandState       : Integer read fCommandState;
    property CommandStateText   : String read getCommandStateText;
    property Successfully       : Boolean read isSuccessfully;
    property CommandType        : Integer read fCommandType;
    property CommandIdent       : TGUID read fCommandIdent;
    property CommandTimeOut     : Cardinal read fCommandTimeOut write fCommandTimeOut;

    property TagDouble          : Double read fTagDouble write fTagDouble;
    property TagString          : String read fTagString write fTagString;

    property OnProcessCommandAnswer   : TBinaryProtocolCommandProcessAnswerEvent
                read fOnProcessCommandAnswer write fOnProcessCommandAnswer;
    property OnProcessCommandStart  : TBinaryProtocolCommandProcessEvent
                read fOnProcessCommandStart write fOnProcessCommandStart;
    property OnProcessCommandFinish : TBinaryProtocolCommandProcessEvent
                read fOnProcessCommandFinish write fOnProcessCommandFinish;
  end;

  // returning Version information from gitversion.inc
  function GetVersionInformation: TExtractFileVersionData;
  function GetVersionNumber: String;

implementation

uses Math;

function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations;
end;

function GetVersionNumber: string;
begin
  Result :=GetVersionInformation.FileVersionData.FileVersionNr;
end;

{ TBinaryProtocolCommand }

class function TBinaryProtocolCommand.checkHeaderIdent(aHeaderData: TBytes; aHeaderIdent : String): Boolean;
var ba : TBytes; len : Integer; zs : String;
begin
  ba := TEncoding.UTF8.GetBytes(aHeaderIdent);
  len := length(ba);
  zs := TEncoding.UTF8.GetString(aHeaderData, 0, len);
  result := zs = aHeaderIdent;
end;

constructor TBinaryProtocolCommand.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  fCommandIdent := TGUID.Empty;
  fCommandType  := 0;
  fCommandState := 0;
  fCommandStateText := '';
  fHeader := TBinaryProtocolCommand.createCommandHeader(fCommandType, fCommandIdent, 0);
  fCommandWaitEvent  := TEvent.Create;
  fCommandWaitEvent.ResetEvent;
  fCommandTimeOut := 5000; // default timeout is 5 seconmds...
  fOnProcessCommandAnswer := nil;
  fHeaderIdentRequest := '';
  fHeaderIdentAnswer  := '';
end;

constructor TBinaryProtocolCommand.CreateIdented(AOwner : TComponent; aIdent : TGUID);
begin
  Create(AOwner);
  self.fCommandIdent := aIdent;
end;

destructor TBinaryProtocolCommand.Destroy;
begin
  fOnProcessCommandAnswer := nil;
  inherited Destroy;
end;

procedure TBinaryProtocolCommand.define(aCommandType: Integer;
    aCommandLength : Integer; aCommandIdent : TGUID);
begin
  fCommandIdent := aCommandIdent;
  fCommandType  := aCommandType;
  fHeader := TBinaryProtocolCommand.createCommandHeader(fCommandType, fCommandIdent, aCommandLength);
end;

procedure TBinaryProtocolCommand.defineHeaderIdents(aHeaderIdentRequest, aHeaderIdentAnswer : String);
begin
  fHeaderIdentRequest := aHeaderIdentRequest;
  fHeaderIdentAnswer  := aHeaderIdentAnswer;
end;

procedure TBinaryProtocolCommand.define(aCommandType: Integer;
    aCommandLength : Integer);
begin
  fCommandIdent := TGUID.NewGuid;
  fCommandType  := aCommandType;
  fHeader := TBinaryProtocolCommand.createCommandHeader(fCommandType, fCommandIdent, aCommandLength);
end;

class function TBinaryProtocolCommand.createCommandHeader(
  aCommandType: Integer; aCommandIdent : TGUID;
  aCommandLength : Integer): RBinaryProtocolCommandHeader;
begin
  result.CommandType  := aCommandType;
  result.CommandIdent := aCommandIdent;
  result.CommandLength:= aCommandLength;
end;

function  TBinaryProtocolCommand.getCommandHeaderLengthRequest : Integer;
var ba : TBytes;
begin
  ba := TEncoding.UTF8.GetBytes(fHeaderIdentRequest);
  result := sizeof(RBinaryProtocolCommandHeader) + length(ba);
end;

function  TBinaryProtocolCommand.getCommandHeaderLengthAnswer : Integer;
var ba : TBytes;
begin
  ba := TEncoding.UTF8.GetBytes(fHeaderIdentAnswer);
  result := sizeof(RBinaryProtocolCommandHeader) + length(ba);
end;

function TBinaryProtocolCommand.getCommandStateText: String;
begin
  case fCommandState of
    NC_BPCOMMANDSTATE_REQUEST : result   := 'request';
    NC_BPCOMMANDSTATE_OK : result        := '';
    NC_BPCOMMANDSTATE_FAIL : result      := 'fail, unknown reason';
    NC_BPCOMMANDSTATE_FAILPROCESSING : result      := 'processing failed, unknown reason';
  else
    result := fCommandStateText;
  end;
end;

procedure TBinaryProtocolCommand.setCommandToFailState(aCommandState : Integer; aCommandStateText : String);
begin
  fCommandState := aCommandState;
  fCommandStateText := aCommandStateText;
end;

function  TBinaryProtocolCommand.processCommandAnswer(aHeaderData, aCommandData : TBytes) : Boolean;
begin
  result := false;
end;

procedure TBinaryProtocolCommand.writeHeaderRequest(aData : TBytes);
var len, lenchreq : Integer; ba : TBytes;
begin
  len := length(aData);
  lenchreq := getCommandHeaderLengthRequest;
  if len >= lenchreq then
  begin
    ba := TEncoding.UTF8.GetBytes(fHeaderIdentRequest);
    Move(ba[0], aData[0], length(ba));
    fHeader.CommandLength   := len - lenchreq;
    fHeader.CommandState    := NC_BPCOMMANDSTATE_REQUEST;
    fHeader.CommandIdent    := fCommandIdent;
    Move(fHeader, aData[length(ba)], lenchreq-length(ba));
  end else
  begin
    raise EBinaryProtocolCommand.Create(Format('Could not write header, data is too short is=%0:d but should>%1:d', [len, getCommandHeaderLengthRequest]));
  end;
end;

procedure TBinaryProtocolCommand.writeHeaderAnswer(aData : TBytes; aState : Integer);
var len, lenchans : Integer; ba : TBytes;
begin
  len := length(aData);
  lenchans := getCommandHeaderLengthAnswer;
  if len >= lenchans then
  begin
    ba := TEncoding.UTF8.GetBytes(fHeaderIdentAnswer);
    Move(ba[0], aData[0], length(ba));
    fHeader.CommandLength   := len - lenchans;
    fHeader.CommandState    := aState;
    fHeader.CommandIdent    := fCommandIdent;
    Move(fHeader, aData[length(ba)], lenchans-length(ba));
  end else
  begin
    raise EBinaryProtocolCommand.Create(Format('Could not write header, data is too short is=%0:d but should>%1:d', [len, getCommandHeaderLengthAnswer]));
  end;
end;

function  TBinaryProtocolCommand.combineRequestData(aData : TBytes) : TBytes;
var ba : TBytes; len, lend : Integer;
begin
  len  := getCommandHeaderLengthRequest;
  lend := length(aData);
  setlength(ba, len + lend);
  writeHeaderRequest(ba);
  if lend > 0 then
  begin
    Move(aData[0], ba[len], lend);
  end;
  result := ba;
end;

function  TBinaryProtocolCommand.combineAnswerData(aData : TBytes) : TBytes;
var ba : TBytes; len, lend : Integer;
begin
  len  := getCommandHeaderLengthAnswer;
  lend := length(aData);
  setlength(ba, len + lend);
  writeHeaderAnswer(ba, fCommandState);
  if lend > 0 then
  begin
    Move(aData[0], ba[len], lend);
  end;
  result := ba;
end;

procedure TBinaryProtocolCommand.waitFor(aTimeOutMS: Cardinal);
var ctimeout : Cardinal; res :  TWaitResult; zsident, zscn : string;
begin
  if aTimeOutMS > 0 then
  begin
    ctimeout := aTimeOutMS;
  end else
  begin
    ctimeout := fCommandTimeOut;
  end;
  res := fCommandWaitEvent.WaitFor(ctimeout);
  if res in [wrTimeout, wrAbandoned, wrError] then
  //if res in [wrTimeout, wrError] then
  begin
    zsident := self.fCommandIdent.ToString;
    zscn := self.ClassName;
    raise EBinaryProtocolCommand.Create(Format('Timeout for command %0:s.%1:s: %2:dms', [zscn, zsident, ctimeout]));
  end;
end;

procedure TBinaryProtocolCommand.informCommandAnswer(aHeaderData, aCommandData : TBytes);
var pheader : PRBinaryProtocolCommandHeader;
begin
  try
    pheader := PRBinaryProtocolCommandHeader(@aHeaderData[length(fHeaderIdentAnswer)]);
    fHeader.CommandState := pheader^.CommandState;
    fCommandState := fHeader.CommandState;
    if not processCommandAnswer(aHeaderData, aCommandData) then
    begin
      if assigned(fOnProcessCommandAnswer) then
      begin
        fOnProcessCommandAnswer(Self, aHeaderData, aCommandData);
      end;
    end;
  finally
    fCommandWaitEvent.SetEvent;
  end;
end;

procedure TBinaryProtocolCommand.informProcessCommandStart(aContext : TObject);
begin
  if Assigned(fOnProcessCommandStart) then
  begin
    fOnProcessCommandStart(self, aContext);
  end;
end;

procedure TBinaryProtocolCommand.informProcessCommandFinish(aContext : TObject);
begin
  if Assigned(fOnProcessCommandFinish) then
  begin
    fOnProcessCommandFinish(self, aContext);
  end;
end;

procedure TBinaryProtocolCommand.informCommandSend;
begin
  fCommandWaitEvent.ResetEvent;
end;


function TBinaryProtocolCommand.isSuccessfully: Boolean;
begin
  result := fCommandState = NC_BPCOMMANDSTATE_OK;
end;

end.
