unit BinaryProtocolClient;

interface

uses
  packages.tools.VersionInformation,
  System.Classes, System.SysUtils, System.Types, BinaryProtocolCommon,
  SyncObjs;

type

  EBinaryProtocolClient = class(Exception)
  public
    CommandClassName      : String;
    CommandIdent          : String;
    CommandStateText      : String;
    CommandState          : Integer;

    constructor Create(const Msg: string; aCommandClassName : string = '';
                      aCommandIdent : string = ''; aStateText : string = ''; aState : integer = 0);
  end;

  TBinaryProtocolClientTransmissionErrorEvent = procedure(Sender : TObject; aErrorText : String) of object;
  TBinaryProtocolClientCommandEvent = procedure(Sender : TObject; Command : TBinaryProtocolCommand) of object;
  TBinaryProtocolClientCommandErrorEvent = procedure(Sender : TObject; Command : TBinaryProtocolCommand; aErrorMessage : String) of object;
  TBinaryProtocolClientDataEvent = procedure(Sender : TObject; const aData : TBytes) of object;

  TBinaryProtocolClient = class(TComponent)
  protected
    fHeaderIdentRequest     : String;
    fHeaderIdentAnswer      : String;
    fPendingCommands        : TList;
    fCSPendingCommands      : TCriticalSection;
    fOnCommandFinished      : TBinaryProtocolClientCommandEvent;
    fOnCommandError         : TBinaryProtocolClientCommandErrorEvent;
    fOnTransmissionError    : TBinaryProtocolClientTransmissionErrorEvent;
    fOnParseError           : TBinaryProtocolClientTransmissionErrorEvent;
    fOnDataSent             : TBinaryProtocolClientDataEvent;
    fOnDataReceived         : TBinaryProtocolClientDataEvent;

    procedure informParseError(aErrorText : String); virtual;
    procedure informTransmissionError(aErrorText : String); virtual;
    procedure informDataSent(const aData : TBytes); virtual;
    procedure informDataReceived(const aData : TBytes); virtual;

    procedure sendData(aData : TBytes); virtual; abstract;
    function  receiveDataHeader(aData : TBytes) : Integer; virtual;
    procedure receiveDataCommand(aHeaderData, aCommandData : TBytes); virtual;

    procedure addPendingCommand(aCommand : TBinaryProtocolCommand); virtual;
    function  isPendingCommand(aCommand : TBinaryProtocolCommand) : Boolean; virtual;
    function  removePendingCommand(aCommand : TBinaryProtocolCommand) : Boolean; virtual;
    function  getPendingCommandIndex(aCommand : TBinaryProtocolCommand) : Integer; virtual;
    function  getPendingCommand(aCommandIdent : TGUID) : TBinaryProtocolCommand; virtual;
    procedure clearPendingCommands; virtual;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function  getCommandHeaderLengthRequest : Integer;
    function  getCommandHeaderLengthAnswer : Integer;

    procedure startCommand(aCommand : TBinaryProtocolCommand); virtual;
    procedure waitForCommand(aCommand : TBinaryProtocolCommand; aCommandTimeOut : Integer = 0); virtual;
    procedure executeCommand(aCommand : TBinaryProtocolCommand; aCommandTimeOut : Integer = 0); virtual;
  published
    property HeaderIdentRequest   : String read fHeaderIdentRequest write fHeaderIdentRequest;
    property HeaderIdentAnswer    : String read fHeaderIdentAnswer write fHeaderIdentAnswer;
    property OnCommandFinished    : TBinaryProtocolClientCommandEvent read fOnCommandFinished write fOnCommandFinished;
    property OnCommandError       : TBinaryProtocolClientCommandErrorEvent read fOnCommandError write fOnCommandError;
    property OnTransmissionError  : TBinaryProtocolClientTransmissionErrorEvent read fOnTransmissionError write fOnTransmissionError;
    property OnParseError         : TBinaryProtocolClientTransmissionErrorEvent read fOnParseError write fOnParseError;
    property OnDataSent           : TBinaryProtocolClientDataEvent read fOnDataSent write fOnDataSent;
    property OnDataReceived       : TBinaryProtocolClientDataEvent read fOnDataReceived write fOnDataReceived;
  end;


  // returning Version information from gitversion.inc
  function GetVersionInformation: TExtractFileVersionData;
  function GetVersionNumber: String;

implementation

function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations;
end;

function GetVersionNumber: string;
begin
  Result :=GetVersionInformation.FileVersionData.FileVersionNr;
end;

{ TBinaryProtocolClient }

constructor TBinaryProtocolClient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fPendingCommands        := TList.Create;
  fCSPendingCommands      := TCriticalSection.Create;
  fOnCommandFinished      := nil;
  fOnCommandError         := nil;
  fOnTransmissionError    := nil;
  fOnParseError           := nil;
end;

destructor TBinaryProtocolClient.Destroy;
begin
  fOnCommandFinished      := nil;
  fOnCommandError         := nil;
  fOnTransmissionError    := nil;
  fOnParseError           := nil;
  clearPendingCommands;
  FreeAndNil(fPendingCommands);
  FreeAndNil(fCSPendingCommands);
  inherited Destroy;
end;

procedure TBinaryProtocolClient.executeCommand(
  aCommand: TBinaryProtocolCommand; aCommandTimeOut : Integer);
begin
  startCommand(aCommand);
  waitForCommand(aCommand, aCommandTimeOut);
end;

procedure TBinaryProtocolClient.addPendingCommand(
  aCommand: TBinaryProtocolCommand);
begin
  if aCommand = nil then
  begin
    raise EBinaryProtocolClient.Create('addPendingCommand needs a non nil command parameter');
  end;
  if not isPendingCommand(aCommand) then
  begin
    fCSPendingCommands.Enter;
    try
      fPendingCommands.Add(aCommand);
    finally
      fCSPendingCommands.Leave;
    end;
  end;
end;

procedure TBinaryProtocolClient.clearPendingCommands;
var i : Integer; cmd : TBinaryProtocolCommand;
begin
  fCSPendingCommands.Enter;
  try
    for i := 0 to fPendingCommands.Count - 1 do
    begin
      cmd := fPendingCommands[i];
      FreeAndNil(cmd);
    end;
    fPendingCommands.Clear;
  finally
    fCSPendingCommands.Leave;
  end;
end;

function TBinaryProtocolClient.isPendingCommand(
  aCommand: TBinaryProtocolCommand): Boolean;
var i : Integer; cmd : TBinaryProtocolCommand;
begin
  if aCommand = nil then
  begin
    raise EBinaryProtocolClient.Create('isPendingCommand needs a non nil command parameter');
  end;
  result := false;
  fCSPendingCommands.Enter;
  try
    for i := 0 to fPendingCommands.Count - 1 do
    begin
      cmd := fPendingCommands[i];
      if cmd.CommandIdent = aCommand.CommandIdent then
      begin
        result := true;
        break;
      end;
    end;
  finally
    fCSPendingCommands.Leave;
  end;
end;

function TBinaryProtocolClient.removePendingCommand(
  aCommand: TBinaryProtocolCommand): Boolean;
var i : Integer; cmd : TBinaryProtocolCommand;
begin
  if aCommand = nil then
  begin
    raise EBinaryProtocolClient.Create('removePendingCommand needs a non nil command parameter');
  end;
  result := false;
  fCSPendingCommands.Enter;
  try
    for i := 0 to fPendingCommands.Count - 1 do
    begin
      cmd := fPendingCommands[i];
      if cmd.CommandIdent = aCommand.CommandIdent then
      begin
        fPendingCommands.Delete(i);
        result := true;
        break;
      end;
    end;
  finally
    fCSPendingCommands.Leave;
  end;
end;

function  TBinaryProtocolClient.getPendingCommandIndex(
  aCommand : TBinaryProtocolCommand) : Integer;
var i : Integer; cmd : TBinaryProtocolCommand;
begin
  if aCommand = nil then
  begin
    raise EBinaryProtocolClient.Create('getPendingCommandIndex needs a non nil command parameter');
  end;
  result := -1;
  fCSPendingCommands.Enter;
  try
    for i := 0 to fPendingCommands.Count - 1 do
    begin
      cmd := fPendingCommands[i];
      if cmd.CommandIdent = aCommand.CommandIdent then
      begin
        result := i;
        break;
      end;
    end;
  finally
    fCSPendingCommands.Leave;
  end;
end;

function  TBinaryProtocolClient.getPendingCommand(aCommandIdent : TGUID) : TBinaryProtocolCommand;
var i : Integer; cmd : TBinaryProtocolCommand;
begin
  result := nil;
  fCSPendingCommands.Enter;
  try
    for i := 0 to fPendingCommands.Count - 1 do
    begin
      cmd := fPendingCommands[i];
      if cmd.CommandIdent = aCommandIdent then
      begin
        result := cmd;
        break;
      end;
    end;
  finally
    fCSPendingCommands.Leave;
  end;
end;

procedure TBinaryProtocolClient.startCommand(
  aCommand: TBinaryProtocolCommand);
var ba : TBytes;
begin
  if aCommand = nil then
  begin
    raise EBinaryProtocolClient.Create('startCommand needs a non nil command parameter');
  end;
  if isPendingCommand(aCommand) then
  begin
    raise EBinaryProtocolClient.Create('startCommand cannot be started twice for same command!');
  end;
  aCommand.defineHeaderIdents(fHeaderIdentRequest, fHeaderIdentAnswer);
  ba := aCommand.toBytesRequest;
  addPendingCommand(aCommand);
  aCommand.informCommandSend;
  try
    sendData(ba);
  except
    on e : exception do
    begin
      removePendingCommand(aCommand);
      raise;
    end;
  end;
end;

procedure TBinaryProtocolClient.waitForCommand(
  aCommand: TBinaryProtocolCommand; aCommandTimeOut : Integer);
var ind : Integer;
begin
  ind := getPendingCommandIndex(aCommand);
  if ind < 0 then
  begin
    raise EBinaryProtocolClient.Create('cannot wait for a non pending command!');
  end;
  try
    try
      aCommand.waitFor(aCommandTimeOut);
    except
      on e : exception do
      begin
        if assigned(fOnCommandError) then
        begin
          fOnCommandError(Self, aCommand, e.Message);
        end;
      end;
    end;
  finally
    removePendingCommand(aCommand);
  end;
  if assigned(fOnCommandFinished) then
  begin
    fOnCommandFinished(Self, aCommand);
  end;
end;

function  TBinaryProtocolClient.receiveDataHeader(aData: TBytes) : Integer;
var len : Integer; pheader : PRBinaryProtocolCommandHeader;
    cmd : TBinaryProtocolCommand;
begin
  len := length(aData);
  if len <> getCommandHeaderLengthAnswer then
  begin
    raise EBinaryProtocolClient.Create(Format('Received data is not correct! is=%0:d byte, should=%1:d byte', [len, sizeof(RBinaryProtocolCommandHeader)]));
  end;
  pheader := PRBinaryProtocolCommandHeader(@aData[length(fHeaderIdentAnswer)]);
  if not TBinaryProtocolCommand.checkHeaderIdent(aData, fHeaderIdentAnswer) then
  begin
    raise EBinaryProtocolClient.Create('Received data has syntax error, wrong header ident');
  end;
  cmd := getPendingCommand(pheader^.CommandIdent);
  if cmd = nil then
  begin
    raise EBinaryProtocolClient.Create(Format('There is no pending command with ident "%0:s"', [pheader^.CommandIdent.ToString]));
  end;
  if cmd.CommandType <> pheader^.CommandType then
  begin
    raise EBinaryProtocolClient.Create(Format('Pending command and answer have different command types pc=%0:d - a=%1:d', [cmd.CommandType, pheader^.CommandType]));
  end;
  result := pheader^.CommandLength;
end;

procedure TBinaryProtocolClient.receiveDataCommand(aHeaderData, aCommandData : TBytes);
var len : Integer; pheader : PRBinaryProtocolCommandHeader;
    cmd : TBinaryProtocolCommand;  datalength : Integer;
begin
  len := length(aHeaderData);
  if len <> getCommandHeaderLengthAnswer then
  begin
    raise EBinaryProtocolClient.Create(Format('Received data is not correct! is=%0:d byte, should=%1:d byte', [len, sizeof(RBinaryProtocolCommandHeader)]));
  end;
  pheader := PRBinaryProtocolCommandHeader(@aHeaderData[length(fHeaderIdentAnswer)]);
  if not TBinaryProtocolCommand.checkHeaderIdent(aHeaderData, fHeaderIdentAnswer) then
  begin
    raise EBinaryProtocolClient.Create('Received data has syntax error, wrong header ident');
  end;
  cmd := getPendingCommand(pheader^.CommandIdent);
  if cmd = nil then
  begin
    raise EBinaryProtocolClient.Create(Format('There is no pending command with ident "%0:s"', [pheader^.CommandIdent.ToString]));
  end;
  if cmd.CommandType <> pheader^.CommandType then
  begin
    raise EBinaryProtocolClient.Create(Format('Pending command and answer have different command types pc=%0:d - a=%1:d', [cmd.CommandType, pheader^.CommandType]));
  end;
  datalength := length(aCommandData);
  if pheader^.CommandLength <> datalength then
  begin
    raise EBinaryProtocolClient.Create(Format('Command data length is of wrong size is=%0:d but should=%1:d', [datalength, pheader^.CommandLength]));
  end;
  cmd.informCommandAnswer(aHeaderData, aCommandData);
end;

function  TBinaryProtocolClient.getCommandHeaderLengthRequest : Integer;
var ba : TBytes;
begin
  ba := TEncoding.UTF8.GetBytes(fHeaderIdentRequest);
  result := sizeof(RBinaryProtocolCommandHeader) + length(ba);
end;

function  TBinaryProtocolClient.getCommandHeaderLengthAnswer : Integer;
var ba : TBytes;
begin
  ba := TEncoding.UTF8.GetBytes(fHeaderIdentAnswer);
  result := sizeof(RBinaryProtocolCommandHeader) + length(ba);
end;

procedure TBinaryProtocolClient.informTransmissionError(aErrorText : String);
begin
  if assigned(fOnTransmissionError) then
  begin
    fOnTransmissionError(Self, aErrorText);
  end;
end;

procedure TBinaryProtocolClient.informParseError(aErrorText : String);
begin
  if assigned(fOnParseError) then
  begin
    fOnParseError(Self, aErrorText);
  end;
end;

procedure TBinaryProtocolClient.informDataSent(const aData : TBytes);
begin
  if assigned(fOnDataSent) then
  begin
    fOnDataSent(self, aData);
  end;
end;

procedure TBinaryProtocolClient.informDataReceived(const aData : TBytes);
begin
  if assigned(fOnDataReceived) then
  begin
    fOnDataReceived(self, aData);
  end;
end;



{ EBinaryProtocolClient }

constructor EBinaryProtocolClient.Create(const Msg: string; aCommandClassName,
  aCommandIdent, aStateText: string; aState: integer);
begin
  inherited Create(Msg);
  CommandClassName := aCommandClassName;
  CommandIdent  := aCommandIdent;
  CommandStateText  := aStateText;
  CommandState  := aState;
end;

end.
