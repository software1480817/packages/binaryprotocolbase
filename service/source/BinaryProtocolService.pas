unit BinaryProtocolService;

interface

uses
  System.Classes, System.SysUtils, System.Types, BinaryProtocolCommon,
  SyncObjs, nxLogging, packages.tools.VersionInformation;

type

  EBinaryProtocolService = class(Exception)
  end;

  TBinaryProtocolServiceCommandFactory = class(TComponent)
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function  createCommandByType(aCommandType : Integer; aCommandIdent : TGUID) : TBinaryProtocolCommand; virtual; abstract;
  end;

  TBinaryProtocolServiceProcessCommandAnswerEvent = procedure(Sender : TObject; aContext : TObject; aCommand : TBinaryProtocolCommand; const aHeaderData, aCommandData : TBytes) of object;
  TBinaryProtocolServiceProcessCommandEvent = procedure(Sender : TObject; aContext : TObject; aCommand : TBinaryProtocolCommand; const aCommandData : TBytes) of object;
  TBinaryProtocolServiceInnerProcessCommandEvent = procedure(Sender : TObject; aContext : TObject; aCommand : TBinaryProtocolCommand) of object;

  TBinaryProtocolService = class(TComponent)
  private
    procedure onProcessCommandAnswerCB(Sender : TObject; aHeaderData, aCommandData : TBytes);
    procedure onProcessCommandStartCB(Sender : TBinaryProtocolCommand; aContext : TObject);
    procedure onProcessCommandFinishCB(Sender : TBinaryProtocolCommand; aContext : TObject);
  protected
    fHeaderIdentRequest         : String;
    fHeaderIdentAnswer          : String;
    fCommandFactory             : TBinaryProtocolServiceCommandFactory;
    fLogger                     : TNxLogger;
    fOnProcessCommandAnswer     : TBinaryProtocolServiceProcessCommandAnswerEvent;
    fOnProcessCommandStart      : TBinaryProtocolServiceProcessCommandEvent;
    fOnProcessCommandFinish     : TBinaryProtocolServiceProcessCommandEvent;
    fOnProcessCommandInnerStart : TBinaryProtocolServiceInnerProcessCommandEvent;
    fOnProcessCommandInnerFinish: TBinaryProtocolServiceInnerProcessCommandEvent;


    function  receiveDataHeader(aData : TBytes) : Integer; virtual;
    function  receiveDataCommand(aHeaderData, aCommandData : TBytes; aContext : TObject) : TBinaryProtocolCommand; virtual;

    function  createCommandByType(aCommandType : Integer; aCommandIdent : TGUID) : TBinaryProtocolCommand; virtual;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function  getCommandHeaderLengthRequest : Integer;
    function  getCommandHeaderLengthAnswer : Integer;

    property LocalLogger    : TNxLogger read fLogger write fLogger;
  published
    property HeaderIdentRequest     : String read fHeaderIdentRequest write fHeaderIdentRequest;
    property HeaderIdentAnswer      : String read fHeaderIdentAnswer write fHeaderIdentAnswer;
    property CommandFactory : TBinaryProtocolServiceCommandFactory read fCommandFactory write fCommandFactory;

    property OnProcessCommandStart      : TBinaryProtocolServiceProcessCommandEvent read fOnProcessCommandStart write fOnProcessCommandStart;
    property OnProcessCommandFinish     : TBinaryProtocolServiceProcessCommandEvent read fOnProcessCommandFinish write fOnProcessCommandFinish;
    property OnProcessCommandInnerStart : TBinaryProtocolServiceInnerProcessCommandEvent read fOnProcessCommandInnerStart write fOnProcessCommandInnerStart;
    property OnProcessCommandInnerFinish: TBinaryProtocolServiceInnerProcessCommandEvent read fOnProcessCommandInnerFinish write fOnProcessCommandInnerFinish;

  end;

  // returning Version information from gitversion.inc
  function GetVersionInformation: TExtractFileVersionData;
  function GetVersionNumber: String;

implementation


function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations;
end;

function GetVersionNumber: string;
begin
  Result :=GetVersionInformation.FileVersionData.FileVersionNr;
end;

{ TBinaryProtocolCommandFactory }

constructor TBinaryProtocolServiceCommandFactory.Create(
  AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TBinaryProtocolServiceCommandFactory.Destroy;
begin
  inherited Destroy;
end;


{ TBinaryProtocolService }

constructor TBinaryProtocolService.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fCommandFactory := nil;
  fLogger := nxLogging.Logger;
end;

function TBinaryProtocolService.createCommandByType(
  aCommandType: Integer; aCommandIdent : TGUID): TBinaryProtocolCommand;
begin
  if fCommandFactory = nil then
  begin
    raise EBinaryProtocolService.Create('No CommandFactory was assigned to the service!');
  end;
  result := fCommandFactory.createCommandByType(aCommandType, aCommandIdent);
  result.OnProcessCommandAnswer := onProcessCommandAnswerCB;
  result.OnProcessCommandStart  := onProcessCommandStartCB;
  result.OnProcessCommandFinish := onProcessCommandFinishCB;
  if result = nil then
  begin
    if fLogger <> nil then if fLogger.isInfo then
        fLogger.info(self.ClassName, Format('command factory could not find command of type "%0:s" with ident "%1:s"', [IntToStr(aCommandType), aCommandIdent.ToString]));
  end;
end;

destructor TBinaryProtocolService.Destroy;
begin
  fCommandFactory := nil;
  inherited Destroy;
end;

procedure TBinaryProtocolService.onProcessCommandAnswerCB(Sender : TObject; aHeaderData, aCommandData : TBytes);
begin
  if Assigned(fOnProcessCommandAnswer) then
  begin
    fOnProcessCommandAnswer(self, nil, Sender as TBinaryProtocolCommand, aHeaderData, aCommandData);
  end;
end;

procedure TBinaryProtocolService.onProcessCommandStartCB(Sender : TBinaryProtocolCommand; aContext : TObject);
begin
  if Assigned(fOnProcessCommandInnerStart) then
  begin
    fOnProcessCommandInnerStart(self, aContext, Sender);
  end;
end;

procedure TBinaryProtocolService.onProcessCommandFinishCB(Sender : TBinaryProtocolCommand; aContext : TObject);
begin
  if Assigned(fOnProcessCommandInnerFinish) then
  begin
    fOnProcessCommandInnerFinish(self, aContext, Sender);
  end;
end;

function TBinaryProtocolService.getCommandHeaderLengthRequest: Integer;
var ba : TBytes;
begin
  ba := TEncoding.UTF8.GetBytes(fHeaderIdentRequest);
  result := sizeof(RBinaryProtocolCommandHeader) + length(ba);
end;

function TBinaryProtocolService.getCommandHeaderLengthAnswer: Integer;
var ba : TBytes;
begin
  ba := TEncoding.UTF8.GetBytes(fHeaderIdentAnswer);
  result := sizeof(RBinaryProtocolCommandHeader) + length(ba);
end;

function  TBinaryProtocolService.receiveDataCommand(aHeaderData,
  aCommandData: TBytes; aContext : TObject) : TBinaryProtocolCommand;
var len       : Integer;
    pheader   : PRBinaryProtocolCommandHeader;
    cmd       : TBinaryProtocolCommand;
    prc       : IBinaryProtocolCommandProcessable;
begin
  len := length(aHeaderData);
  if len <> getCommandHeaderLengthRequest then
  begin
    raise EBinaryProtocolService.Create(Format('Received (Service) data is not correct! is=%0:d byte, should=%1:d byte', [len, getCommandHeaderLengthRequest]));
  end;
  pheader := PRBinaryProtocolCommandHeader(@aHeaderData[length(fHeaderIdentRequest)]);
  if not TBinaryProtocolCommand.checkHeaderIdent(aHeaderData, fHeaderIdentRequest) then
  begin
    raise EBinaryProtocolService.Create('Received (Service) data has syntax error, wrong header ident');
  end;
  if fLogger <> nil then
  begin
    if fLogger.isTrace then
    begin
      fLogger.trace(self.ClassName, Format('got command: magic=%0:s, type=%1:s, len=%2:s, ident=%3:s, state=%4:s',
            [fHeaderIdentRequest, IntToStr(pheader^.CommandType), IntToStr(pheader^.CommandLength), pheader^.CommandIdent.ToString, IntToStr(pheader^.CommandState)]));
    end;
  end;
  cmd := createCommandByType(pheader^.CommandType, pheader^.CommandIdent);
  if cmd = nil then
  begin
    raise EBinaryProtocolService.Create('Unknown command type (Service): factory results nil');
  end;
  if cmd.GetInterface(IBinaryProtocolCommandProcessable, prc) then
  begin
    try
      if fLogger <> nil then
      begin
        if fLogger.isTrace then
        begin
          fLogger.trace(self.ClassName, Format('processing command "%0:s.%1:s[%2:d]"...',
              [cmd.ClassName, cmd.CommandIdent.ToString, cmd.CommandType]));
        end;
      end;
      if Assigned(OnProcessCommandStart) then
      begin
        OnProcessCommandStart(self, aContext, cmd, aCommandData);
      end;
      prc.process(aContext, aCommandData);
      if fLogger <> nil then
      begin
        if cmd.isSuccessfully then
        begin
          if fLogger.isTrace then
          begin
            fLogger.trace(self.ClassName, Format('command "%0:s.%1:s[%2:d]" processed successfully',
                [cmd.ClassName, cmd.CommandIdent.ToString, cmd.CommandType]));
          end;
        end else
        begin
          if fLogger.isInfo then
          begin
            fLogger.info(self.ClassName, Format('command "%0:s.%1:s[%2:d]" processing FAIL %3:d - %4:s',
                [cmd.ClassName, cmd.CommandIdent.ToString, cmd.CommandType, cmd.CommandState, cmd.CommandStateText]));
          end;
        end;
      end;
      if Assigned(OnProcessCommandFinish) then
      begin
        OnProcessCommandFinish(self, aContext, cmd, aCommandData);
      end;
    except
      on e : exception do
      begin
        cmd.setCommandToFailState(NC_BPCOMMANDSTATE_FAILPROCESSING, e.Message);
        if fLogger.isError then
        begin
          fLogger.error(self.ClassName, Format('command "%0:s.%1:s[%2:d]" processing EXCEPTION "%3:s" %4:d - %5:s',
              [cmd.ClassName, cmd.CommandIdent.ToString, cmd.CommandType, e.Message, cmd.CommandState, cmd.CommandStateText]));
        end;
      end;
    end;
  end else
  begin
    if fLogger <> nil then
    begin
      if fLogger.isDebug then
      begin
        fLogger.debug(self.ClassName, Format('the command "%0:s.%1:s[%2:d]" does not support interface "IBinaryProtocolCommandProcessable", no processing possible',
            [cmd.ClassName, cmd.CommandIdent.ToString, cmd.CommandType]));
      end;
    end;
  end;
  result := cmd;
end;

function TBinaryProtocolService.receiveDataHeader(aData: TBytes): Integer;
var len : Integer; pheader : PRBinaryProtocolCommandHeader;
begin
  len := length(aData);
  if len <> getCommandHeaderLengthRequest then
  begin
    raise EBinaryProtocolService.Create(Format('Received (Service) data is not correct! is=%0:d byte, should=%1:d byte', [len, getCommandHeaderLengthRequest]));
  end;
  pheader := PRBinaryProtocolCommandHeader(@aData[length(fHeaderIdentRequest)]);
  if not TBinaryProtocolCommand.checkHeaderIdent(aData, fHeaderIdentRequest) then
  begin
    raise EBinaryProtocolService.Create('Received (Service) data has syntax error, wrong header ident');
  end;
  result := pheader^.CommandLength;
  if fLogger <> nil then
  begin
    if fLogger.isTrace then
    begin
      fLogger.trace(self.ClassName, Format('got command header: magic=%0:s, type=%1:s, len=%2:s, ident=%3:s, state=%4:s',
            [fHeaderIdentRequest, IntToStr(pheader^.CommandType), IntToStr(pheader^.CommandLength), pheader^.CommandIdent.ToString, IntToStr(pheader^.CommandState)]));
    end;
  end;
end;





end.
