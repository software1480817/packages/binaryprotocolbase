
pipeline {
    agent { label 'Windows_Jenkins_Agent' }

    options {
        // remove old builds
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '10', daysToKeepStr: '', numToKeepStr: '10')
        // set timestamp in logs
        timestamps()
    }

    environment {
        DEBUG_MODE         = "false"
        SHOW_INFO          = "false"
        PROJ_GROUP         = 'opBinaryProtocolBaseGroupDX10Rio.groupproj'
        PROJ_CONFIG        = 'Release'
        PROJ_PLATFORM32    = 'Win32'
        PROJ_PLATFORM64    = 'Win64'        
        PROJ_SUBDIR        = '\\projects\\DX10Rio'      
        OUTPUT_DIR         = '\\output\\DX10Rio'   
        ARTIFACTS_DIR      =  "\\Artifacts"
        VersionNr          = getVersionNumber()
        VersionName        = getVersionNumberName()
        VersionType        = getVersionType()

        /* make adjustments in
            - stage "provide artifacts" : to provide your artifacts
            - post: 
	            - failure: emailext: send email
         */
    }    
  
    stages {
        
        /////////////////////////////////////////////////////
        // Stage: Show Info
        /////////////////////////////////////////////////////
        stage("Set and show Info") {   
            steps{ 
                script {
                    if (getVersionNumber() == "") {
                        ShowInfo()
                        currentBuild.result = 'ABORTED'
                        error('Version Number not found!')
                    }
                    else {
                        currentBuild.displayName = VersionName
                    }
                }

                script {
                    if (SHOW_INFO == "true") {
                        ShowInfo()
                    }
                }                               
            }
        }

        /////////////////////////////////////////////////////
        // Stage: MSBuild
        /////////////////////////////////////////////////////
        stage("MSBuild") {
            environment {
                PROJ_DEFINES = "\"\"" // set defines when it is needed, e.g. "\"EUREKALOG;CI;E_RELEASEMODE\""
            }             
            steps{
                echo '************* MSBuild 32Bit ************* '
                bat '"' + BUILD_HELPER_DELPHI + '" ' + WORKSPACE + ' ' + PROJ_GROUP + ' ' + PROJ_CONFIG + ' ' + PROJ_PLATFORM32 + ' ' + PROJ_DEFINES
                echo '************* MSBuild 64Bit************* '
                bat '"' + BUILD_HELPER_DELPHI + '" ' + WORKSPACE + ' ' + PROJ_GROUP + ' ' + PROJ_CONFIG + ' ' + PROJ_PLATFORM64 + ' ' + PROJ_DEFINES
            }                                                                                               
        }    
    }

    post {   
        failure {
            echo '************* POST failure *************'
            script {
                if (DEBUG_MODE == "false") {
                    emailext attachLog: true, 
                            body: '$DEFAULT_CONTENT', 
                            recipientProviders: [developers(), requestor()], 
                            subject: '$DEFAULT_SUBJECT' , 
                            recipients: emailextrecipients([buildUser(), developers(), requestor()])
                            //to: '$BUILD_RECIPIENTS' // Jenkins admins
                            // see https://www.jenkins.io/doc/pipeline/steps/email-ext/
                }       
            }           
        }
        cleanup {
            echo '************* POST cleanup *************'
            script {
                try { // cleaning Workspace. otherwise can be used asynch cleanWs
                    script {                
                        if (DEBUG_MODE == "false") {
                            deleteDir()
                        }
                    }
                }
                catch(Exception e) {
                    echo 'Error: Cannot Clean output: ' + e.toString()
                }   
            }          
        }             
    } 
}


def getVersionNumberName() {
    return 'V' + getVersionNumber()
}

def getVersionNumber() {


    // expected TAG V1.2.3
    // Result: 1.2.3
    /* testet variations
        " " | "V1.2.3." | "Version1.2.3." | "V1.2.3" | "1.2.3." | "1.2.3"
    */

    vVers = getCommandOutput("git describe --tags --abbrev=0")

    // remove all spaces
    vVers = vVers.replace(' ', '');
    if (vVers.length() == 0) {
        vResult = ""
    }
    else { // find from TAG name the first number
        /* debugging 
        for(int i in 0..vVers.length() -1) { 
            println "[" + i + "]: " + vVers[i]
        }   
        */
        for(int i in 0..vVers.length() -1) { 
            if (vVers[i].isNumber()) {
                //println "i: " + i + " | " + vVers[i]
                vResult = vVers.substring(i, vVers.length())
                break
            }
        }      

        if ( ! vResult[vResult.length()-1].isNumber()) { // 1.2.3. ends with . => remove it
            vResult = vResult.substring(0, vResult.length()-1)
        }  

         vResult = vResult + '.' + currentBuild.number      
    }

    return vResult // returning e.g. 1.2.3.267
}

def getSetupFileName(Prefix) {
    return 'Setup_' + Prefix + '_' + VersionName // e.g. Setup_Tool_V3.15.0.exe
}

def getVersionType() {
    // 3: VersionType: 1: Release, 2: Testversion, 3: Nightlybuild, 4: Hotfix-Testversion, 5: Featurebuild
    // only Release supportet for Tools
    return '1'
}

def getCommandOutput(cmd) {
    script{
        echo "getCommandOutput: " + cmd
        if (isUnix()){
            return sh(returnStdout:true , script: cmd).trim()
        } else{
            stdout = bat(returnStdout:true , script: cmd).trim()
            result = stdout.readLines().drop(1).join(" ")   
            echo "... " + result    
            return result
        } 
    }
} 


def ShowInfo() {
    echo '************* Show Info *************'              
    echo 'JENKINS_HOME: ' + JENKINS_HOME + ' | BUILD_HELPER_DELPHI: ' + BUILD_HELPER_DELPHI + ' | WORKSPACE: ' + WORKSPACE
    echo 'PROJ_GROUP: ' + PROJ_GROUP + ' | PROJ_CONFIG: ' + PROJ_CONFIG
    echo 'JOB_NAME: ' + JOB_NAME + ' | JOB_URL: ' + JOB_URL + ' | BUILD_NUMBER: ' + BUILD_NUMBER + ' | BUILD_TAG: ' + BUILD_TAG + ' | GIT_COMMIT: ' + GIT_COMMIT
    echo 'OPTOOLS: ' + OPTOOLS + ' | CHECKEUREKALOGUSES: ' + CHECKEUREKALOGUSES     
    echo 'VersionNr: ' + VersionNr +  ' | displayName: ' + currentBuild.displayName 
}